<?php

namespace Drupal\Tests\entity_form_monitor\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Simple test to ensure that main page loads with module installed.
 *
 * @group entity_form_monitor
 */
class EntityMonitorTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'system',
    'field',
    'filter',
    'user',
    'node',
    'entity_form_monitor',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The node used for testing.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * The logged in user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Create a page content type.
    $this->drupalCreateContentType(['type' => 'page', 'name' => 'Basic page']);
    $this->node = $this->drupalCreateNode();

    // Create a user with required permissions and log them in.
    $this->user = $this->drupalCreateUser([
      'bypass node access',
    ]);
    $this->drupalLogin($this->user);
  }

  /**
   * Tests that data attributes are added to the form correctly.
   */
  public function testFormAttributes() {
    // Test that the node add form does not have the monitoring attributes,
    // since it is a new, unsaved entity.
    $this->drupalGet('node/add/page');
    $form = $this->assertSession()->elementExists('css', 'form.node-form');
    $this->assertFalse($form->hasAttribute('data-entity-form-monitor'));
    $this->assertFalse($form->hasAttribute('data-entity-last-changed'));

    // Test that the node edit form has the monitoring attributes.
    $this->drupalGet('node/' . $this->node->id() . '/edit');
    $form = $this->assertSession()->elementExists('css', 'form.node-form');
    $this->assertEquals('node:' . $this->node->id(), $form->getAttribute('data-entity-form-monitor'));
    $this->assertEquals($this->node->getChangedTime(), $form->getAttribute('data-entity-last-changed'));

    // Test that the user edit form has the monitoring attributes.
    $this->drupalGet('user/' . $this->user->id() . '/edit');
    $form = $this->assertSession()->elementExists('css', 'form.user-form');
    $this->assertEquals('user:' . $this->user->id(), $form->getAttribute('data-entity-form-monitor'));
    $this->assertEquals($this->user->getChangedTime(), $form->getAttribute('data-entity-last-changed'));

    // Disable monitoring for the page node type and the user entity type.
    $this->config('entity_form_monitor.settings')
      ->set('entities', ['node:article'])
      ->save();

    // Test that the node edit form does not have the monitoring attributes.
    $this->drupalGet('node/' . $this->node->id() . '/edit');
    $form = $this->assertSession()->elementExists('css', 'form.node-form');
    $this->assertFalse($form->hasAttribute('data-entity-form-monitor'));
    $this->assertFalse($form->hasAttribute('data-entity-last-changed'));

    // Test that the user edit form does not have the monitoring attributes.
    $this->drupalGet('user/' . $this->user->id() . '/edit');
    $form = $this->assertSession()->elementExists('css', 'form.user-form');
    $this->assertFalse($form->hasAttribute('data-entity-form-monitor'));
    $this->assertFalse($form->hasAttribute('data-entity-last-changed'));
  }

}
